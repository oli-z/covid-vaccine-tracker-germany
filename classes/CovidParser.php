<?php

namespace NerdsClub\CovidVaccineTrackerGermany;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CovidParser
{
	/**
	 * @var Spreadsheet The PhpSpreadsheet spreadsheet object.
	 */
	private $spreadsheet;
	/**
	 * @var \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet|null The worksheet obkect containing the needed data.
	 */
	private $worksheet;
	/**
	 * @var array An array of all states available.
	 */
	private $states = [];

	/**
	 * @var int The column containing the name of the states.
	 */
	protected $statecolumn = 1;
	/**
	 * @var string[] An array containing possible indications.
	 */
	private $indicationcolumns = [
		'Indikation nach Alter*',
		'Berufliche Indikation*',
		'Medizinische Indikation*',
		'Pflegeheim-bewohnerIn*'
	];

	/**
	 * CovidParser constructor.
	 * @param string $filename Path of the file to load.
	 */
	public function __construct($filename) {
		$this->spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);
		$this->worksheet = $this->getDataWorksheet($this->spreadsheet);
		$this->states = $this->getStates($this->worksheet);
		if (!$this->selfTest()) {
			throw new \Exception("Error parsing the file.");
		}
	}

	/**
	 * @return int Should be int if everything goes right.
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getTotal() {
		$row = $this->searchRowWithValue($this->worksheet, $this->statecolumn, "Gesamt");
		$column = $this->searchColumnWithValue($this->worksheet, 1, "Impfungen kumulativ");
		return ($this->worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue());
	}

	/**
	 * @return int Should be int if everything goes right.
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getTotalDifference() {
		$row = $this->searchRowWithValue($this->worksheet, $this->statecolumn, "Gesamt");
		$column = $this->searchColumnWithValue($this->worksheet, 1, "Differenz zum Vortag");
		return ($this->worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue());
	}

	/**
	 * @return array
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getTotalIndication() {
		$indications = [];
		foreach ($this->indicationcolumns as $indicationcolumn) {
			$col = $this->searchColumnWithValue($this->worksheet, 1, $indicationcolumn);
			$row = $this->searchRowWithValue($this->worksheet, $this->statecolumn, "Gesamt");
			if ($col && $row) {
				$value = $this->worksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
				if ($value && $value != "") {
					$indicationcolumn = rtrim($indicationcolumn, "*");
					$indications[] = [$indicationcolumn, $value];
				}
			}

		}
		return $indications;
	}

	/**
	 * @param string $state The state for which you want the data for.
	 * @return int Should be int if everything goes right.
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getStateTotal($state) {
		$row = $this->searchRowWithValue($this->worksheet, $this->statecolumn, $state);
		$column = $this->searchColumnWithValue($this->worksheet, 1, "Impfungen kumulativ");
		return ($this->worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue());
	}

	/**
	 * @param string $state The state for which you want the data for.
	 * @return int Should be int if everything goes right.
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getStateDifference($state) {
		$row = $this->searchRowWithValue($this->worksheet, $this->statecolumn, $state);
		$column = $this->searchColumnWithValue($this->worksheet, 1, "Differenz zum Vortag");
		return ($this->worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue());
	}

	/**
	 * @param string $state The state for which you want the data for.
	 * @return array
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getStateIndication($state) {
		$indications = [];
		foreach ($this->indicationcolumns as $indicationcolumn) {
			$col = $this->searchColumnWithValue($this->worksheet, 1, $indicationcolumn);
			$row = $this->searchRowWithValue($this->worksheet, $this->statecolumn, $state);
			if ($col && $row) {
				$value = $this->worksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
				if ($value && $value != "") {
					$indicationcolumn = rtrim($indicationcolumn, "*");
					$indications[] = [$indicationcolumn, $value];
				}
			}

		}
		return $indications;
	}

	/**
	 * @return array Array containing all the states.
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	public function getStates() {
		$states = [];
		$row = 2;
		do {
			$value = $this->worksheet->getCellByColumnAndRow($this->statecolumn, $row)->getCalculatedValue();
			if ($value != "Gesamt" && $value != "") {
				$row++;
				$go = true;
				$states[] = $value;
			} else {
				$go = false;
			}
		} while ($go);

		return $states;
	}

	/**
	 * @param $spreadsheet The spreadsheet in which the function should search for the worksheet with the data.
	 * @return Worksheet Returns worksheet objcet of the worksheet containing the data.
	 * @throws \PhpOffice\PhpSpreadsheet\Exception
	 */
	private function getDataWorksheet($spreadsheet) {
		$worksheets = $spreadsheet->getSheetNames();
		foreach ($worksheets as $worksheet) {
			if (preg_match("/.*\d{2,4}.\d{2}.\d{2,4}.*/", $worksheet)) {
				return $this->spreadsheet->getSheetByName($worksheet);
			}
		}
		return $this->spreadsheet->getSheet($this->spreadsheet->getSheetCount() - 1);
	}

	/**
	 * @param Worksheet $worksheet The worksheet to search for data.
	 * @param int $column The column in which you want to search.
	 * @param mixed $value The value you want to search for.
	 * @param int $limit Limit the amount of rows to search through. Defaults to 1000.
	 * @return false|int Row number or false if not found.
	 */
	private function searchRowWithValue($worksheet, $column, $value, $limit = 1000) {
		$row = 1;
		do {
			if ($worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue() == $value) {
				return $row;
			} else {
				$row++;
			}
		} while ($row < $limit);
		return false;
	}

	/**
	 * @param Worksheet $worksheet The worksheet to search for data.
	 * @param int $row The row in which you want to search.
	 * @param mixed $value The value you want to search for.
	 * @param int $limit Limit the amount of rows to search through. Defaults to 1000.
	 * @return false|int Column number or false if not found.
	 */
	private function searchColumnWithValue($worksheet, $row, $value, $limit = 1000) {
		$column = 0;
		do {
			if ($worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue() == $value) {
				return $column;
			} else {
				$column++;
			}
		} while ($column < $limit);
		return false;
	}

	/**
	 * Checks if the parsing works correctly by checking some values.
	 * @return bool True if succeeds, false if fails.
	 * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
	 */
	private function selfTest() {
		if (
			$this->getTotal() > 0 &&
			$this->getTotalDifference() > 0 &&
			count($this->states) > 2 &&
			!is_numeric($this->states[0]) &&
			$this->getStateTotal($this->states[0]) > 0 &&
			$this->getStateDifference($this->states[0]) > 0
		) return true; else return false;
	}
}