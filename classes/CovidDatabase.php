<?php


namespace NerdsClub\CovidVaccineTrackerGermany;


use PDO;

class CovidDatabase {

	/**
	 * CovidDatabase constructor.
	 * @param string $filename
	 */
	public $db;

	public function __construct($filename) {
		if (!file_exists($filename)) {
			$this->db = new PDO('sqlite:' . $filename);

			$create_sql ='
				BEGIN TRANSACTION;
				CREATE TABLE IF NOT EXISTS "days" (
					"day"	INTEGER NOT NULL UNIQUE,
					"total_vaccines"	INTEGER,
					"vaccines_diff"	INTEGER,
					PRIMARY KEY("day")
				);
				CREATE TABLE IF NOT EXISTS "indications" (
					"iid"	INTEGER NOT NULL UNIQUE,
					"name"	TEXT,
					PRIMARY KEY("iid" AUTOINCREMENT)
				);
				CREATE TABLE IF NOT EXISTS "day_indications" (
					"day"	INTEGER NOT NULL,
					"iid"	INTEGER NOT NULL,
					"value"	INTEGER,
					FOREIGN KEY("day") REFERENCES "days"("day"),
					FOREIGN KEY("iid") REFERENCES "indications"("iid"),
					PRIMARY KEY("day","iid")
				);
				CREATE TABLE IF NOT EXISTS "state_days" (
					"day"	INTEGER NOT NULL,
					"sid"	INTEGER NOT NULL,
					"total_vaccines"	INTEGER,
					"vaccines_diff"	INTEGER,
					FOREIGN KEY("sid") REFERENCES "states"("sid"),
					FOREIGN KEY("day") REFERENCES "days"("day"),
					PRIMARY KEY("sid","day")
				);
				CREATE TABLE IF NOT EXISTS "states" (
					"sid"	INTEGER NOT NULL UNIQUE,
					"name"	TEXT,
					PRIMARY KEY("sid" AUTOINCREMENT)
				);
				CREATE TABLE IF NOT EXISTS "state_day_indications" (
					"sid"	INTEGER NOT NULL,
					"day"	INTEGER NOT NULL,
					"iid"	INTEGER NOT NULL,
					"value"	INTEGER,
					FOREIGN KEY("sid") REFERENCES "states"("sid"),
					FOREIGN KEY("iid") REFERENCES "indications"("iid"),
					FOREIGN KEY("day") REFERENCES "days"("day"),
					PRIMARY KEY("iid","day","sid")
				);
				COMMIT;
			';

			$this->db->exec($create_sql);
		}
		else {
			// Verbindung
			$this->db = new PDO('sqlite:' . $filename);
		}
	}

	/**
	 * @param CovidParser|CovidParser2 $parser
	 * @param int $date
	 */
	public function update($parser, $date) {
		$stmt = $this->db->prepare("SELECT * FROM days WHERE day = ?");
		$stmt->execute([$date]);
		$days_db = $stmt->fetchAll();

		// if the day doesn't exist yet create the data for it
		if(count($days_db) == 0) {
			$stmt = $this->db->prepare("INSERT INTO days (day, total_vaccines, vaccines_diff) VALUES (?, ?, ?)");
			$stmt->execute([$date, $parser->getTotal(), $parser->getTotalDifference()]);
			$day_id = $this->db->lastInsertId();

			$indictations = $parser->getTotalIndication();

			foreach($indictations as $indictation) {
				$indictation_name = $indictation[0];
				$indictation_value = $indictation[1];

				$stmt = $this->db->prepare("SELECT iid FROM indications WHERE name = ?");
				$stmt->execute([$indictation_name]);
				$res = $stmt->fetch(PDO::FETCH_ASSOC);
				if($res) {
					$iid = $res['iid'];
				} else {
					$stmt = $this->db->prepare("INSERT INTO indications (name) VALUES (?)");
					$stmt->execute([$indictation_name]);
					$iid = $this->db->lastInsertId();
				}

				$stmt = $this->db->prepare("INSERT INTO day_indications (day, iid, value) VALUES (?, ?, ?)");
				$stmt->execute([$date, $iid, $indictation_value]);
			}

			$states = $parser->getStates();

			foreach($states as $state) {
				$stmt = $this->db->prepare("SELECT sid FROM states WHERE name = ?");
				$stmt->execute([$state]);
				$res = $stmt->fetch(PDO::FETCH_ASSOC);
				if($res) {
					$sid = $res["sid"];
				} else {
					$stmt =$this->db->prepare("INSERT INTO states (name) VALUES (?)");
					$stmt->execute([$state]);
					$sid = $this->db->lastInsertId();
				}

				$stmt = $this->db->prepare("INSERT INTO state_days (day, sid, total_vaccines, vaccines_diff) VALUES (?, ?, ?, ?)");

				$state_total = $parser->getStateTotal($state);
				$state_diff = $parser->getStateDifference($state);

				$stmt->execute([$date, $sid, $state_total, $state_diff]);
				$state_indications = $parser->getStateIndication($state);
				foreach($state_indications as $indictation) {
					$indictation_name = $indictation[0];
					$indictation_value = $indictation[1];

					$stmt = $this->db->prepare("SELECT iid FROM indications WHERE name = ?");
					$stmt->execute([$indictation_name]);
					$res = $stmt->fetch(PDO::FETCH_ASSOC);
					if($res) {
						$iid = $res['iid'];
					} else {
						$stmt = $this->db->prepare("INSERT INTO indications (name) VALUES (?)");
						$stmt->execute([$indictation_name]);
						$iid = $this->db->lastInsertId();
					}

					$stmt = $this->db->prepare("INSERT INTO state_day_indications (sid, day, iid, value) VALUES (?, ?, ?, ?)");
					$stmt->execute([$sid, $date, $iid, $indictation_value]);
				}
			}
		}
	}

	/**
	 * @return array
	 */
	public function getStates() {
		$stmt = $this->db->query("SELECT name FROM states");
		return array_column($stmt->fetchAll(PDO::FETCH_ASSOC), "name");
	}

	public function getDays() {
		$stmt = $this->db->query("SELECT day FROM days ORDER BY day DESC");
		return array_column($stmt->fetchAll(PDO::FETCH_ASSOC), "day");
	}

	public function getDayCounters($day) {
		$stmt = $this->db->prepare("SELECT total_vaccines, vaccines_diff FROM days  WHERE day = ?");
		$stmt->execute([$day]);
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function getDayIndications($day) {
		$stmt = $this->db->prepare("SELECT name, value FROM days
    										JOIN day_indications ON days.day = day_indications.day
    										JOIN indications On day_indications.iid = indications.iid
											WHERE days.day = ?");
		$stmt->execute([$day]);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getStateDayCounters($state, $day) {
		$stmt = $this->db->prepare("SELECT state_days.total_vaccines, state_days.vaccines_diff FROM days 
											JOIN state_days ON days.day = state_days.day
											JOIN states ON state_days.sid = states.sid
											WHERE days.day = ? AND states.name = ?");
		$stmt->execute([$day, $state]);
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public function getStateDayIndications($state, $day) {
		$stmt = $this->db->prepare("SELECT indications.name, state_day_indications.value FROM days 
											JOIN state_days ON days.day = state_days.day
											JOIN states ON state_days.sid = states.sid
											JOIN state_day_indications ON state_day_indications.sid = states.sid AND state_day_indications.day = days.day	
											JOIN indications ON indications.iid = state_day_indications.iid	
											WHERE days.day = ? AND states.name = ?");
		$stmt->execute([$day, $state]);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}
