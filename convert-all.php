<?php
/* Helper script to convert existing XLSXs to JSONs */

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

error_reporting(E_ALL);
ini_set("display_errors", 1);

$days = array_diff(scandir("data_raw"), array('..', '.', '.gitkeep'));
foreach ($days as $filename) {
    $filename = rtrim($filename, ".xlsx");
    echo $filename;
    if (!file_exists("data_parsed/" . $filename . ".json")) {
        //if(1) {    
        echo ("JSON not found, converting...");
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("data_raw/" . $filename . ".xlsx");
        $worksheet = $spreadsheet->getSheet($spreadsheet->getSheetCount() - 1);
        $row = 2;
        $data = [];
        do {
            $blname = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
            //$bl = [];
            //$bl["blname"] = $blname;
            $col = 2;
            do {
                $key = rtrim($worksheet->getCellByColumnAndRow($col, 1)->getValue(), "*");
                $value = $worksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
                if ($value != "" && $key != "") {
                    $data[$blname][$key] = $value;
                    //$bl[$key] = $value;
                }
                $col++;
            } while ($worksheet->getCellByColumnAndRow($col, 1)->getValue() != "");
            //$data[] = $bl;
            $row++;
            $blname = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
        } while ($blname != "");
        echo "<pre>";
        $json = json_encode($data, JSON_PRETTY_PRINT);
        file_put_contents("data_parsed/" . $filename . ".json", $json);
        echo $json;
    }
}
