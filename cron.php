<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
$page = file_get_contents("https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Impfquotenmonitoring");
$basedomain = "https://www.rki.de";
$pagedom = new DOMDocument();
$pagedom->loadHTML($page);

$xpath = new DOMXPath($pagedom);

$classname = "text";
if (preg_match('/(?<=\(Datenstand\s)\d+\.\d+\.\d+,\s\d+:\d+/', $xpath->query("//*[contains(concat(' ', normalize-space(@id), ' '), ' main ')]")[0]->nodeValue, $datenstand)) {
    //var_dump($datenstand);
    $datenstand = strtotime($datenstand[0]);
} else {
    throw new Exception("Page structure has changed.");
}
$filename = date("Y-m-d", $datenstand);

$dllink = $basedomain . $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' more downloadLink InternalLink ')]")[0]->getAttribute("href");

if (!file_exists("data_raw/" . $filename . ".xlsx")) {
    echo ("XLSX not found, downloading...<br/>");
    file_put_contents("data_raw/" . $filename . ".xlsx", file_get_contents($dllink));
    require_once 'convert.php';
}