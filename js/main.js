var states = [];

function getGraphColor(index)
{
    return ["#ae2a2a", "#68b31e", "#319973", "#5335ba",
        "#83ba35", "#ab1e5b", "#63ee6b", "#b1b1b1",
        "#3f72ea", "#c3b647"][index % 10];
}

window.onload = function () {
    fetch('api/getAllData')
        .then(response => response.json())
        .then(data => {
            var firstKey = Object.keys(data["data"])[0];
            var keys = Object.keys(data["data"]);
            var latest = data["data"][firstKey];
            var states_totals_arr = [];
            var states_totals_labels = [];
			var stand = new Date(firstKey);
			
            //var over_time_arr = [];

            document.querySelector("#stand").innerHTML = stand.toLocaleDateString('de-DE', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });

            document.querySelector("#total").innerHTML = latest["Gesamt"]["Impfungen kumulativ"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            var percentage = latest["Gesamt"]["Impfungen kumulativ"] / 83000000 * 100;
            document.querySelector("#percentage").innerHTML = (Math.round(percentage * 1000) / 1000).toString() + " %";
            document.querySelector("#new").innerHTML = "+ " + latest["Gesamt"]["Differenz zum Vortag"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

            Object.keys(latest).forEach(key => {
                if (key != "Gesamt")
                {
                    states_totals_arr.push(latest[key]["Impfungen kumulativ"]);    // fill array for chart
                    states_totals_labels.push(key);
                }
                states.indexOf(key) === -1 ? states.push(key) : true;                                                   // fill 'states' array, maybe we need it later
                //over_time_arr[key] = ({name: key, data: []});  // for later
                //state["name"] = key;
                //console.log(state);
            });

            var over_time_arr = {};     // we start with an object because it's like an associative array
            console.log(keys);
            keys.forEach(daykey => {
                Object.keys(data["data"][daykey]).forEach((statekey, index) => {
                    statekey_name = statekey.replace("Gesamt", "Total");
                    over_time_arr[statekey] = over_time_arr[statekey] || { label: statekey_name, backgroundColor: "transparent", borderColor: getGraphColor(index), data: [] };
                    over_time_arr[statekey]["data"].push(data["data"][daykey][statekey]["Impfungen kumulativ"]);
                })
            });
            states.forEach(state => {
                over_time_arr[state]["data"] = over_time_arr[state]["data"].reverse();
            })
            over_time_arr = Object.values(over_time_arr);   // convert object back to array because that's what the chart library needs

            new Chart(document.getElementById("over-time").getContext("2d"), {
                type: "line",
                data: {
                    labels: keys,
                    datasets: over_time_arr,
                    options: {
                        responsive: true
                    }
                }
            });

            new Chart(document.getElementById("by-state").getContext("2d"), {
                type: "pie",
                data: {
                    datasets: [{
                        data: states_totals_arr,
                        backgroundColor: new Array(16).fill(0).map((v, i) => getGraphColor(i))
                    }],
                    labels: states_totals_labels,
                    options: {
                        responsive: true
                    }
                }
            });

            document.querySelector("#loading").classList.add("hidden");
            document.querySelector("#main").classList.remove("background-loading");
        });

}