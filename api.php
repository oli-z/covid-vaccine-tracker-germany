<?php

include 'vendor/autoload.php';

use NerdsClub\CovidVaccineTrackerGermany\CovidDatabase;

$db = new CovidDatabase("data.db");
$states = $db->getStates();
$available_dates = $db->getDays();
$return = [];
$return["status"] = "ok";
$return["error"] = "";

$query = $_GET["query"];

$return["query"] = $query;

if ($query == "") {
	header("Location: https://documenter.getpostman.com/view/14040857/TVt2bihY");
	die();
} elseif  ($query === "getDataByDate") {
	if (isset($_GET["date"]) && $_GET["date"] != "" && strtotime($_GET["date"])) {
		$return["parameters"]["date"] = $_GET["date"];
		$date = strtotime($_GET["date"]);
		if(in_array($date, $available_dates)) {
			foreach ($states as $state) {
				$state_arr = [];
				$state_stats = $db->getStateDayCounters($state, $date);
				$state_arr["Impfungen kumulativ"] = intval($state_stats["total_vaccines"]);
				$state_arr["Differenz zum Vortag"] = intval($state_stats["vaccines_diff"]);

				$inds = $db->getStateDayIndications($state, $date);

				foreach ($inds as $ind) {
					$state_arr[$ind["name"]] = intval($ind["value"]);
				}

				$return["data"][$state] = $state_arr;

			}

			$day_total = $db->getDayCounters($date);
			$return["data"]["Gesamt"]["Impfungen kumulativ"] = intval($day_total["total_vaccines"]);
			$return["data"]["Gesamt"]["Differenz zum Vortag"] = intval($day_total["vaccines_diff"]);
			$inds = $db->getDayIndications($date);
			foreach ($inds as $ind) {
				$return["data"]["Gesamt"][$ind["name"]] = intval($ind["value"]);
			}
		} else {
			$return["status"] = "error";
			$return["error"] = "No data found for given day.";
		}
	} else {
		$return["status"] = "error";
		$return["error"] = "Parameter 'date' not given or in wrong format.";
	}
} elseif ($query === "getAllData") {
	foreach($available_dates as $date) {
		$date_ymd = date("Y-m-d", $date);
		foreach ($states as $state) {
			$state_arr = [];
			$state_stats = $db->getStateDayCounters($state, $date);
			$state_arr["Impfungen kumulativ"] = intval($state_stats["total_vaccines"]);
			$state_arr["Differenz zum Vortag"] = intval($state_stats["vaccines_diff"]);

			$inds = $db->getStateDayIndications($state, $date);

			foreach ($inds as $ind) {
				$state_arr[$ind["name"]] = intval($ind["value"]);
			}
			$day_arr[$state] = $state_arr;
		}

		$day_total = $db->getDayCounters($date);
		$day_arr["Gesamt"]["Impfungen kumulativ"] = intval($day_total["total_vaccines"]);
		$day_arr["Gesamt"]["Differenz zum Vortag"] = intval($day_total["vaccines_diff"]);
		$inds = $db->getDayIndications($date);
		foreach ($inds as $ind) {
			$day_arr["Gesamt"][$ind["name"]] = intval($ind["value"]);
		}
		$return["data"][$date_ymd] = $day_arr;
	}
} else {
	$return["status"] = "error";
	$return["error"] = "Unknown query.";
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Content-Type: application/json');
echo (json_encode($return, JSON_PRETTY_PRINT));