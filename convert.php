<pre>
<?php
require 'vendor/autoload.php';

use NerdsClub\CovidVaccineTrackerGermany\CovidParser;
use NerdsClub\CovidVaccineTrackerGermany\CovidParser2;
use NerdsClub\CovidVaccineTrackerGermany\CovidDatabase;

$files_raw = array_diff(scandir("data_raw"), array('..', '.', '.gitkeep'));

$db = new CovidDatabase("data.db");

foreach ($files_raw as $file_raw) {
	if(preg_match("/\d{4}-\d{2}-\d{2}\.xlsx/", $file_raw)) {
		$filename = "data_raw/" . $file_raw;
		$date = strtotime(rtrim($file_raw, ".xlsx"));

		if($date >= strtotime("2021-01-07")) {
			$parser = new CovidParser2($filename);
		} else {
			$parser = new CovidParser($filename);
		}

		$db->update($parser, $date);
	}
}
